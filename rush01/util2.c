/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 20:32:17 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/31 22:39:19 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_set_config(int di, int cfg[4])
{
	cfg[0] = 0;
	cfg[1] = 0;
	cfg[2] = di;
	if (di == 4)
		cfg[3] = 2;
	else
		cfg[3] = 3;
}

void	ft_input_board_top(char **board, int col, int di)
{
	int	i;

	i = 0;
	while (i < di)
	{
		board[i][col] = (i + 1) + '0';
		i++;
	}
}

void	ft_input_board_down(char **board, int col, int di)
{
	int	i;
	int	j;

	i = 0;
	j = di;
	while (i < di)
	{
		board[i][col] = j + '0';
		i++;
		j--;
	}
}

void	ft_input_board_left(char **board, int row, int di)
{
	int	i;

	i = 0;
	while (i < di)
	{
		board[row][i] = (i + 1) + '0';
		i++;
	}
}

void	ft_input_board_right(char **board, int row, int di)
{
	int	i;
	int	j;

	i = 0;
	j = di;
	while (i < di)
	{
		board[row][i] = j + '0';
		i++;
		j--;
	}
}
