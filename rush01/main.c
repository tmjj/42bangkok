/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 19:08:37 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/31 22:20:05 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void	ft_create_board(char ***arr, int row, int col);
void	ft_trim_input(char *in, char *out);
void	ft_input_puzzel(char **puz, char *input, int di);
void	ft_set_config(int di, int cfg[4]);
void	ft_input_board(char **board, char *input, int cfg[4]);
void	ft_print_board(char **board, int di);
int		ft_solve_board(char **board, char **puzzel, int cfg[4]);
int		ft_get_dimension(char *inp);

int	main(int ac, char *av[])
{
	char	**board;
	char	**puzzel;
	char	*input_trim;
	int		cfg[4];
	int		di;

	if (ac != 2)
	{
		write(1, "Input error", 12);
		return (0);
	}
	di = ft_get_dimension(av[1]);
	ft_create_board(&board, di, di);
	ft_create_board(&puzzel, 4, di);
	input_trim = (char *)malloc((di * 4) * sizeof(char));
	ft_trim_input(av[1], input_trim);
	ft_input_puzzel(puzzel, input_trim, di);
	ft_set_config(di, cfg);
	ft_input_board(board, input_trim, cfg);
	if (ft_solve_board(board, puzzel, cfg) == 1)
		ft_print_board(board, di);
	else
		write(1, "No solution", 13);
	return (0);
}
