/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 19:14:16 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/31 20:58:06 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

void	ft_input_board_top(char **board, int col, int di);
void	ft_input_board_down(char **board, int col, int di);
void	ft_input_board_left(char **board, int row, int di);
void	ft_input_board_right(char **board, int row, int di);

int	ft_get_dimension(char *inp)
{
	int	i;
	int	di;

	i = 0;
	di = 0;
	while (inp[i] != '\0')
	{
		if ((inp[i] >= '0' && inp[i] <= '9') || inp[i] == ' ')
		{
			if (inp[i] != ' ')
				di++;
		}
		else
			return (0);
		i++;
	}
	return ((di / 2) / 2);
}

void	ft_create_board(char ***arr, int row, int col)
{
	char	**tmp;
	int		i;
	int		j;

	i = 0;
	tmp = (char **)malloc(row * sizeof(char *));
	while (i < row)
	{
		tmp[i] = (char *)malloc(col * sizeof(char));
		j = 0;
		while (j < col)
		{
			tmp[i][j] = '0';
			j++;
		}
		i++;
	}
	*arr = tmp;
}

void	ft_trim_input(char *in, char *out)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (in[i] != '\0')
	{
		if (in[i] >= '0' && in[i] <= '9')
		{
			out[j] = in[i];
			j++;
		}
		i++;
	}
}

void	ft_input_puzzel(char **puz, char *in, int di)
{
	int	i;

	i = 0;
	while (i < (di * 4))
	{
		puz[i / di][i % di] = in[i];
		i++;
	}
}

void	ft_input_board(char **board, char *input, int cfg[4])
{
	int	i;

	i = 0;
	while (i < (cfg[2] * 4))
	{
		if (input[i] == cfg[2] + '0')
		{
			if (i / cfg[2] == 0)
				ft_input_board_top(board, i % cfg[2], cfg[2]);
			if (i / cfg[2] == 1)
				ft_input_board_down(board, i % cfg[2], cfg[2]);
			if (i / cfg[2] == 2)
				ft_input_board_left(board, i % cfg[2], cfg[2]);
			if (i / cfg[2] == 3)
				ft_input_board_right(board, i % cfg[2], cfg[2]);
		}
		i++;
	}
}
