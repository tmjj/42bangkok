/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 21:01:52 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/31 22:44:10 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_is_number_in_row(char **board, char number, int cfg[4])
{
	int	i;

	i = 0;
	while (i < cfg[2])
	{
		if (board[cfg[0]][i] == number)
			return (1);
		i++;
	}
	return (0);
}

int	ft_is_number_in_col(char **board, char number, int cfg[4])
{
	int	i;

	i = 0;
	while (i < cfg[2])
	{
		if (board[i][cfg[1]] == number)
			return (1);
		i++;
	}
	return (0);
}

int	ft_is_number_in_box(char **board, char number, int cfg[4])
{
	int	lrow;
	int	lcol;
	int	i;
	int	j;

	lrow = cfg[0] - cfg[0] % cfg[3];
	lcol = cfg[1] - cfg[1] % cfg[3];
	i = lrow;
	while (i < cfg[3] + lrow)
	{
		j = lcol;
		while (j < cfg[3] + lcol)
		{
			if (board[i][j] == number)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int	ft_is_invalid_horizontal(char **board, char **puzzel, int cfg[4])
{
	int	i;
	int	s;
	int	e;
	int	tmp;

	i = 0;
	s = 1;
	e = 1;
	tmp = 0;
	if (cfg[1] == (cfg[2] - 1))
	{
		while (i < cfg[2] -1)
		{
			if (board[cfg[0]][tmp] < board[cfg[0]][i + 1])
			{
				s++;
				tmp = i + 1;
			}
			i++;
		}
		i = cfg[2] - 1;
		tmp = cfg[2] - 1;
		while (i != 0)
		{
			if (board[cfg[0]][tmp] < board[cfg[0]][i - 1])
			{
				tmp = i - 1;
				e++;
			}
			i--;
		}
		if (puzzel[2][cfg[0]] == (s + '0') && puzzel[3][cfg[0]] == (e + '0'))
			return (0);
		else
			return (1);
	}
	else
		return (0);
}

int	ft_is_invalid_vertical(char **board, char **puzzel, int cfg[4])
{
	int	i;
	int	s;
	int	e;
	int	tmp;

	i = 0;
	s = 1;
	e = 1;
	tmp = 0;
	if (cfg[0] == (cfg[2] - 1))
	{
		while (i < cfg[2] -1)
		{
			if (board[tmp][cfg[1]] < board[i + 1][cfg[1]])
			{
				s++;
				tmp = i + 1;
			}
			i++;
		}
		i = cfg[2] - 1;
		tmp = cfg[2] - 1;
		while (i != 0)
		{
			if (board[tmp][cfg[1]] < board[i - 1][cfg[1]])
			{
				tmp = i -1;
				e++;
			}
			i--;
		}
		if (puzzel[0][cfg[1]] == (s + '0') && puzzel[1][cfg[1]] == (e + '0'))
			return (0);
		else
			return (1);
	}
	else
		return (0);
}

int	ft_is_valid_place(char **board, char **puzzel, char number, int cfg[4])
{
	int	rowok;
	int	colok;
	int	boxok;
	int	ver;
	int	hol;

	ver = 0;
	hol = 0;
	rowok = !ft_is_number_in_row(board, number, cfg);
	colok = !ft_is_number_in_col(board, number, cfg);
	boxok = !ft_is_number_in_box(board, number, cfg);
	if (rowok && colok && boxok)
	{
		if (cfg[0] == (cfg[2] - 1))
		{
			board[cfg[0]][cfg[1]] = number;
			ver = ft_is_invalid_vertical(board, puzzel, cfg);
			board[cfg[0]][cfg[1]] = '0';
		}
		if (cfg[1] == (cfg[2] - 1))
		{
			board[cfg[0]][cfg[1]] = number;
			hol = ft_is_invalid_horizontal(board, puzzel, cfg);
			board[cfg[0]][cfg[1]] = '0';
		}
	}
	return (rowok && colok && boxok && !hol && !ver);
}

void	ft_print_board(char **board, int di)
{
	int	i;
	int	j;

	i = 0;
	while (i < di)
	{
		j = 0;
		while (j < di)
		{
			ft_putchar(board[i][j]);
			ft_putchar(' ');
			j++;
		}
		i++;
		ft_putchar('\n');
	}
	write(1, "---------------\n", 16);
}

int	ft_solve_board(char **board, char **puzzel, int cfg[4])
{
	char	num_try;
	int		i;
	int		j;

	i = 0;
	while (i < cfg[2])
	{
		j = 0;
		while (j < cfg[2])
		{
			if (board[i][j] == '0')
			{
				num_try = '1';
				while (num_try <= (cfg[2] + '0'))
				{
					cfg[0] = i;
					cfg[1] = j;
					if (ft_is_valid_place(board, puzzel, num_try, cfg))
					{
						board[i][j] = num_try;
						ft_print_board(board, cfg[2]);
						if (ft_solve_board(board, puzzel, cfg))
							return (1);
						else
							board[i][j] = '0';
					}
					num_try++;
				}
				return (0);
			}
			j++;
		}
		i++;
	}
	return (1);
}
