/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 16:28:02 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/28 22:14:46 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int ft_str_is_printable(char *str);

int main (void)
{
	char s[5];
	s[0] = 35;
	s[1] = 41;
	s[2] = 61;
	s[3] = 91;
	s[4] = 124;
	char t[0];
	char u[5];
	u[0] = 32;
	u[1] = 31;
	u[2] = 61;
	u[3] = 91;
	u[4] = 127;
	int x = ft_str_is_printable(s);
	int y = ft_str_is_printable(t);
	int z = ft_str_is_printable(u);
	printf("result = %d" , x);
	printf("result = %d" , y);
	printf("result = %d" , z);
	int result;
    	result = ft_str_is_printable("asdf");
        printf("Result: %d\n", result);
 	result = ft_str_is_printable("QWERTY");
    printf("Result: %d\n", result);
    result = ft_str_is_printable("asd f1234~");
       printf("Result: %d\n", result);
    result = ft_str_is_printable("999");
       printf("Result: %d\n", result);
    result = ft_str_is_printable("");
        printf("Result: %d\n", result);
}
