/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 16:28:02 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/28 20:33:42 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int ft_str_is_lowercase(char *str);

int main (void)
{
	char *s = "abcdefg";
	char t[0];
	char *u = "asdfSdfasd";
	int x = ft_str_is_lowercase(s);
	int y = ft_str_is_lowercase(t);
	int z = ft_str_is_lowercase(u);
	printf("result = %d" , x);
	printf("result = %d" , y);
	printf("result = %d" , z);
}
