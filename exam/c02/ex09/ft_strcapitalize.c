/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 08:49:30 by cananwat          #+#    #+#             */
/*   Updated: 2021/11/01 09:52:03 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_is_eng_letter(char c)
{
	if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
		return (1);
	return (0);
}

char	ft_uppercase(char c)
{
	if (c >= 'a' && c <= 'z')
		return (c - 32);
	return (c);
}

char	*ft_strcapitalize(char *str)
{
	int	i;
	int	is_letter;

	i = 0;
	is_letter = 0;
	while (str[i])
	{
		is_letter = ft_is_eng_letter(str[i]);
		if (i == 0 && is_letter == 1)
		{
			str[i] = ft_uppercase(str[i]);
		}
		else
		{
			if (str[i - 1] == ' ' && is_letter == 1)
				str[i] = ft_uppercase(str[i]);
		}
		i++;
	}
	return (str);
}
