/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 08:37:31 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/29 08:49:22 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strcapitalize(char *str);

int	main(void)
{
	char s[62] = "salut, comment tu vas ? 42mots quarante-deux; cinquante+et+un";
	printf("before=> %s\n", s);
	ft_strcapitalize(s);
	printf("after=> %s\n", s);
}
