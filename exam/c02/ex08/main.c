/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 08:37:31 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/29 08:43:05 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strlowcase(char *str);

int	main(void)
{
	char s[27] = "aRbcXdXefg";
	printf("before=> %s\n", s);
	ft_strlowcase(s);
	printf("after=> %s\n", s);
}
