/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test13.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 14:02:24 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/27 16:10:46 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include "ex00/ft_strcpy.c"
#include "ex01/ft_strncpy.c"
#include "ex02/ft_str_is_alpha.c"
#include "ex03/ft_str_is_numeric.c"

int	main(void)
{
	char *s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char d[27];
	printf("src -> %s \n" , s);
	ft_strcpy(d,s);
	printf("dest -> %s \n\n" , d);

	char *s1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char d1[27];
	printf("src -> [%s] \n" , s1);
	ft_strncpy(d1,s1,10);
	printf("dest -> [%s] \n\n" , d1);

}
