/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 16:28:02 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/28 20:48:01 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int ft_str_is_uppercase(char *str);

int main (void)
{
	char *s = "ABCDEF";
	char t[0];
	char *u = "asdSSSfSdfasd";
	int x = ft_str_is_uppercase(s);
	int y = ft_str_is_uppercase(t);
	int z = ft_str_is_uppercase(u);
	printf("result = %d" , x);
	printf("result = %d" , y);
	printf("result = %d" , z);
}
