/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 16:28:02 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/28 16:38:06 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int ft_str_is_numeric(char *str);

int main (void)
{
	char *s = "1234567890";
	char t[0];
	char *u = "123s3333";
	int x = ft_str_is_numeric(s);
	int y = ft_str_is_numeric(t);
	int z = ft_str_is_numeric(u);
	printf("result = %d" , x);
	printf("result = %d" , y);
	printf("result = %d" , z);
}
