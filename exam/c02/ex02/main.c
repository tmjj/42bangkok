/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 15:35:28 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/28 16:20:21 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int ft_str_is_alpha(char *str);

int main(void)
{
	char *s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	char t[0];
	char *u = "ijur$";
	int x;
	int y;
	int z;
        x = ft_str_is_alpha(s);
	printf("result = %d\n" , x);
        y = ft_str_is_alpha(t);
	printf("result = %d\n" , y);
        z = ft_str_is_alpha(u);
	printf("result = %d\n" , z);
}
