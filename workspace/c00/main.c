/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/21 15:37:24 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/28 09:42:27 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
//#include "ex00/ft_putchar.c"
#include "ex01/ft_print_alphabet.c"
#include "ex02/ft_print_reverse_alphabet.c"
#include "ex03/ft_print_numbers.c"
#include "ex04/ft_is_negative.c"
#include "ex05/ft_print_comb.c"
#include "ex06/ft_print_comb2.c"

int	main(void)
{
	/*
	ft_putchar('c');
	ft_putchar('\n');
	ft_print_alphabet();
	ft_putchar('\n');
	ft_print_reverse_alphabet();
	ft_putchar('\n');
	ft_print_numbers();
	ft_putchar('\n');
	
	ft_putchar('\n');
	int	x;
	ft_is_negative(x);
	ft_putchar('\n');
	ft_is_negative(-9);
	ft_putchar('\n');
	ft_is_negative(9);
	ft_putchar('\n');
	*/
	//ft_print_comb();
	//ft_putchar('\n');
	ft_print_comb2();
	return (0);
}
