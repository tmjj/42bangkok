/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/27 22:28:12 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/28 08:25:30 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char d[], int islast)
{
	ft_putchar(d[0]);
	ft_putchar(d[1]);
	ft_putchar(d[2]);
	if (islast)
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void	ft_increment(char d[])
{
	if (d[2] == '9')
	{
		if (d[1] == '8')
		{
			d[0]++;
			d[1] = d[0] + 1;
			d[2] = d[1] + 1;
		}
		else
		{
			d[1]++;
			d[2] = d[1] + 1;
		}	
	}
	else
	{
		d[2]++;
	}
}

void	ft_show_str(char d[])
{
	if (d[0] == '7' && d[1] == '8' && d[2] == '9')
	{
		ft_putstr(d, 0);
		return ;
	}
	ft_putstr(d, 1);
	ft_increment(d);
	ft_show_str(d);
}

void	ft_print_comb(void)
{
	char	d[3];
	int		i;

	i = 0;
	while (i < 3)
	{
		d[i] = i + '0';
		i++;
	}
	ft_show_str(d);
}
