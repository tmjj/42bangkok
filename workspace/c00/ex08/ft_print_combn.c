/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 13:53:58 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/24 14:09:35 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_show_str(int n, int *note, int pos)
{
	int	i;

	i = 0;
	if (pos == 1)
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
	while (i < n)
	{
		ft_putchar(note[i] + '0');
		i++;
	}
}

void	ft_print_combn_increment(int n, int *note)
{
	int	i;
	int	max;

	i = n - 1;
	max = 9;
	while (note[i] == max)
	{
		i = i - 1;
		max = max - 1;
	}
	note[i] = note[i] + 1;
	while (i < n)
	{
		note[i + 1] = note[i] + 1;
		i++;
	}
}

void	ft_print_combn(int n)
{
	int	note[10];
	int	i;

	i = 0;
	while (i < n)
	{
		note[i] = i;
		i++;
	}
	ft_show_str(n, note, 0);
	while (note[0] != 10 - n || note[n - 1] != 9)
	{
		if (note[n - 1] != 9)
		{
			note[n - 1]++;
		}
		else
		{
			ft_print_combn_increment(n, note);
		}
		ft_show_str(n, note, 1);
	}
}
