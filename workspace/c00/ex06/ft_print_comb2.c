/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 08:38:51 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/28 09:41:48 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

void	ft_format_str(char d[], int islast)
{
	ft_putchar(d[0]);
	ft_putchar(d[1]);
	ft_putchar(' ');
	ft_putchar(d[2]);
	ft_putchar(d[3]);
	if (islast)
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void	ft_increment_ab(char d[])
{
	if (d[1] == '9')
	{
		d[0]++;
		d[1] = '0';
		d[2] = d[0];
		d[3] = '1';
	}
	else
	{
		d[1]++;
		if (d[1] == '9')
		{
			d[2] = d[0] + 1;
			d[3] = '0';
		}
		else
		{
			d[2] = d[0];
			d[3] = d[1] + 1;
		}
	}
}

void	ft_increment_cd(char d[])
{
	if (d[2] == '9' && d[3] == '9')
	{
		ft_increment_ab(d);
	}
	else
	{
		if (d[3] == '9')
		{
			d[2]++;
			d[3] = '0';
		}
		else
		{
			d[3]++;
		}
	}
}

void	ft_print_str(char d[])
{
	ft_increment_cd(d);
	if (d[0] == '9' && d[1] == '8' && d[2] == '9' && d[3] == '9')
	{		
		ft_format_str(d, 0);
		return ;
	}
	ft_format_str(d, 1);
	ft_print_str(d);
}

void	ft_print_comb2(void)
{
	char	d[4];
	int		i;

	i = 0;
	while (i < 4)
	{
		d[i] = '0';
		i++;
	}
	ft_print_str(d);
}
