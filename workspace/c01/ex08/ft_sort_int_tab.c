/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 16:05:56 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/25 22:13:31 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_swap_arr(int *tab, int start, int end)
{
	int	tmp;

	if (tab[start] < tab[end])
		return ;
	tmp = tab[start];
	tab[start] = tab[end];
	tab[end] = tmp;
}

void	ft_bbsort(int *tab, int start, int end, int size)
{
	if (end == size)
		return ;
	ft_swap_arr(tab, start, end);
	ft_bbsort(tab, start + 1, end + 1, size);
}

void	ft_sort_int_tab(int *tab, int size)
{
	int	index;

	index = 0;
	while (index < size - 1)
	{
		ft_bbsort(tab, 0, 1, size);
		index++;
	}
}
