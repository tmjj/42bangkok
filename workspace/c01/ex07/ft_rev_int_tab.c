/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_int_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 21:42:40 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/25 22:08:29 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_reverse_array(int *tab, int start, int end)
{
	int	tmp;

	if (start > end)
		return ;
	tmp = tab[start];
	tab[start] = tab[end];
	tab[end] = tmp;
	ft_reverse_array(tab, start + 1, end - 1);
}

void	ft_rev_int_tab(int *tab, int size)
{
	ft_reverse_array(tab, 0, size - 1);
}
