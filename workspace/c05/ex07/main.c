#include <stdio.h>

int ft_find_next_prime(int nb);

int main(void)
{
	printf("%d\n", ft_find_next_prime(2147483600));
	printf("%d\n", ft_find_next_prime(71));
	printf("%d\n", ft_find_next_prime(40));
}
