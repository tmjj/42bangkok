/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 22:13:52 by cananwat          #+#    #+#             */
/*   Updated: 2021/11/08 22:17:40 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int	ft_iterative_power(int nb, int power);

int main(void)
{
	printf("%d\n", ft_iterative_power(910, -3185));
	printf("%d\n", ft_iterative_power(0, 0));
	printf("%d\n", ft_iterative_power(2379, 0));
	printf("%d\n", ft_iterative_power(-1027, 1));
	printf("%d\n", ft_iterative_power(0, 2));
	printf("%d\n", ft_iterative_power(3,3));
	printf("%d\n", ft_iterative_power(6, 4));
	printf("%d\n", ft_iterative_power(8, 5));
	printf("%d\n", ft_iterative_power(-7, 6));
	printf("%d\n", ft_iterative_power(-4, 7));
	printf("%d\n", ft_iterative_power(-3, 8));
}
