/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/09 21:05:16 by cananwat          #+#    #+#             */
/*   Updated: 2021/11/09 21:15:15 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int	size;
	int	i;
	int	*arr;

	size = max - min;
	if (size < 1)
		return (0);
	arr = (int *)malloc(sizeof(int) * size);
	if (arr == NULL)
	{
		*range = NULL;
		return (-1);
	}
	else
	{
		i = 0;
		while (i < size)
		{
			arr[i] = min;
			min++;
			i++;
		}
		*range = arr;
		return (i);
	}
}
