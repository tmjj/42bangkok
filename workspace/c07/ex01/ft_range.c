/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/09 20:39:31 by cananwat          #+#    #+#             */
/*   Updated: 2021/11/09 20:53:56 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int	*arr;
	int	size;
	int	i;

	size = max - min;
	if (size < 1)
		return (NULL);
	arr = (int *)malloc(sizeof(int) * size);
	if (arr == NULL)
		return (NULL);
	else
	{
		i = 0;
		while (i < size)
		{
			arr[i] = min;
			i++;
			min++;
		}
		return (arr);
	}
}
