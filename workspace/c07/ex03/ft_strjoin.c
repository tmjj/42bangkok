/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 08:48:20 by cananwat          #+#    #+#             */
/*   Updated: 2021/11/10 11:15:56 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	ft_copy(char *des, char **strs, char *sep)
{
	int	i;
	int	j;
	int	k;

	i = -1;
	k = 0;
	while (strs[++i])
	{
		j = -1;
		while (strs[i][++j])
		{
			des[k++] = strs[i][j];
		}
		if (strs[i + 1])
		{
			j = -1;
			while (sep[++j])
				des[k++] = sep[j];
		}
	}
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	char	*des;
	int		deslen;
	int		i;

	if (size == 0)
	{
		des = (char *)malloc(sizeof(char) * 1);
		return (des);
	}
	i = 0;
	while (strs[++i])
		deslen = deslen + ft_strlen(strs[i]);
	deslen = deslen + (ft_strlen(sep) * size);
	des = (char *)malloc(sizeof(char) * deslen);
	ft_copy(des, strs, sep);
	return (des);
}
