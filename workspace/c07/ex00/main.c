#include <stdio.h>

char *ft_strdup(char *src);

int main(void)
{
	char *str = "hello";
	char *des;

	des = ft_strdup(str);
	printf("%p\n" , str);
	printf("%p\n" , des);

}
