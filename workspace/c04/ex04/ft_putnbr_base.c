/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/03 09:22:09 by cananwat          #+#    #+#             */
/*   Updated: 2021/11/03 10:58:31 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_is_invalid_base(char *base)
{
	int	i;
	int	j;

	i = 0;
	while (base[i] != '\0')
	{
		j = i + 1;
		if (base[i] == '-' || base[i] == '+'
			|| base[i] == '*' || base[i] == '/')
			return (1);
		while (base[j] != '\0')
		{
			if (base[i] == base[j])
				return (1);
			j++;
		}
		i++;
	}
	if (i < 2)
		return (1);
	else
		return (0);
}

void	ft_putnbr_base(int nbr, char *base)
{
	long	nb;
	int		mbase;

	if (!ft_is_invalid_base(base))
	{
		mbase = 0;
		nb = nbr;
		while (base[mbase])
			mbase++;
		if (nb < 0)
		{
			ft_putchar('-');
			nb = nb * -1;
		}
		if (nb < mbase)
		{
			ft_putchar(base[nb]);
		}
		if (nb >= mbase)
		{
			ft_putnbr_base(nb / mbase, base);
			ft_putnbr_base(nb % mbase, base);
		}
	}
}
