/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_combn.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 08:44:42 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/24 09:12:00 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void	display(int db[], int start, int end)
{
	int	i;

	i = 0;
	if (start > end)
		return ;
	while (i < 10)
	{
		printf("%d", db[i]);
		display(db, (start + 1), end);
		i++;
	}
}

void	print_combn(int n)
{
	int db[10];
	int i;

	i = 0;
	while (i < 10)
	{
		db[i] = i;
		i++;
	}
	
	display(db, 1, n);
}

int	main(void)
{
	print_combn(2);
	return (0);
}
