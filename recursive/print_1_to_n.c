/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_1_to_n.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 07:56:41 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/24 08:40:56 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void	ft_print_1_to_n(int n)
{
	if (n == 0)
		return ;
	printf("%d",n);
       	ft_print_1_to_n(n-1);
}
void	ft_xx(int s, int n)
{
	printf("%d", s);
	if (s < n)
		ft_xx(s+1, n);
}
int	main(void)
{
	ft_print_1_to_n(5);
	ft_xx(1,5);
	return (0);
}
