/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_factorial.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 07:45:08 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/24 07:53:33 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int	ft_factorial(int n)
{
	if (n == 1 || n == 0)
		return 1;
	return n * ft_factorial(n-1);
}

int	main(void)
{
	int x = ft_factorial(5);
	printf("%d",x);
}
