/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_for.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 11:17:25 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/23 19:13:49 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_for_x_to_1(int x)
{
	if (x > 0)
	{
		ft_putchar(x + 48);
		ft_for_x_to_1(x - 1);
	}
}

void	ft_for_1_to_x(int x)
{
	if (x > 1)
	{
		ft_for_1_to_x(x - 1);
	}
	ft_putchar(x + 48);
}

int	main(void)
{
	//ft_for_x_to_1(4);
	//ft_putchar('\n');
	ft_for_1_to_x(4);
	return (0);
}
