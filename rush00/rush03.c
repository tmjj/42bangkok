/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ruch03.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 08:56:36 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/23 11:13:31 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

void	ft_printHorizontal(int x, char start, char middle, char end)
{
	if (x-- > 0)
	{
		ft_putchar(start);
		while (x >= 2)
		{
			ft_putchar(middle);
			x--;
		}
		if (x == 1)
			ft_putchar(end);
		ft_putchar('\n');
	}
}

void	ft_printVertical(int x, int y, char verticalBar)
{
	while (y-- > 2)
	{
		int tmp;

		tmp = x;
		ft_putchar(verticalBar);
		tmp = tmp - 1;
		while (tmp >= 2)
		{
			ft_putchar(' ');
			tmp--;
		}
		if (tmp == 1)
			ft_putchar(verticalBar);
		ft_putchar('\n');
	}
}

void	rush03(int x, int y)
{
	ft_printHorizontal(x, 'A', 'B', 'C');
	ft_printVertical(x, y, 'B');
	if (y > 1)
		ft_printHorizontal(x, 'A', 'B', 'C');
}
