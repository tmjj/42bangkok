#ifdef linux
# include <bsd/string.h> // with -lbsd
#endif
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include "../ex02/ft_str_is_alpha.c"

int main()

{
	char salpha[] = "BONJour";
	char snalpha[] = "bonJ our";

	printf("%d salpha\n", ft_str_is_alpha(salpha));
	printf("%d snalpha\n", ft_str_is_alpha(snalpha));
}
