#ifdef linux
# include <bsd/string.h> // with -lbsd
#endif
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include "../ex00/ft_strcpy.c"

int main()

{
	char dest[13];
	char src[] = "aonjour \0 asdf";
	ft_strcpy(dest, src);
	for (int i = 0; i < 13; i++)
		printf("%d ", dest[i]);
	printf("\n%s\n", dest);
	strcpy(dest, "");
	strcpy(dest, src);
	for (int i = 0; i < 13; i++)
		printf("%d ", dest[i]);
	printf("\n%s\n", dest);
}
