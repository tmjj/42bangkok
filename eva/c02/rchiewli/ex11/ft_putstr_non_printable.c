/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rchiewli <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/27 20:57:20 by rchiewli          #+#    #+#             */
/*   Updated: 2021/11/01 16:36:31 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr_non_printable(char *str)
{
	unsigned char	tmp;
	char			*hex_ref;
	unsigned char	*chk;
printf(" => %s" , str);
	hex_ref = "0123456789abcdef";
	chk = (unsigned char *) str;
printf(" => %s" ,chk);
	while (*chk != '\0')
	{
		if (*chk >= ' ' && *chk <= '~')
			write (1, chk, 1);
		else
		{
			ft_putchar('\\');
			tmp = *chk / 16;
			ft_putchar(hex_ref[tmp]);
			tmp = *chk % 16;
			ft_putchar(hex_ref[tmp]);
		}
		chk++;
	}
}
