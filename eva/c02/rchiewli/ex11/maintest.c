#include <string.h>
#include <stdio.h>
#include <limits.h>
#include "../ex11/ft_putstr_non_printable.c"

int main()
{
	char unp[15] = "\t";
	//char unp[15] = "\a \b \t \n \v \f \r \e";

	ft_putstr_non_printable(unp);
	printf("\n");

	/*
	char unp2[1] = {'\xff'};

	for (int i = 0; i < 256; i++)
		unp2[0] = (char)i;
		ft_putstr_non_printable(unp2);
		printf("\n");
		ft_putstr_non_printable("\a \b \t \n \v \f \r \e");
	*/
}
