#ifdef linux
# include <bsd/string.h> // with -lbsd
#endif
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include "../ex03/ft_str_is_numeric.c"

int main()

{
	char salpha[] = "BONJour";
	char snalpha[] = "1234567";

	printf("%d salpha\n", ft_str_is_numeric(salpha));
	printf("%d snalpha\n", ft_str_is_numeric(snalpha));
}
