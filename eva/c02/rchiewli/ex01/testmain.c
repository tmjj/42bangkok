#ifdef linux
# include <bsd/string.h> // with -lbsd
#endif
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include "../ex01/ft_strncpy.c"

int main()

{
	char ndest[15];
	char nsrc[20] = "bonjour jew\0";
	ft_strncpy(ndest, nsrc, sizeof ndest);
	for (int i = 0; i < sizeof ndest; i++)
		printf("%d ", ndest[i]);
	printf("\n%s\n", ndest);
	strncpy(ndest, "eqwrwer", sizeof ndest);
	strncpy(ndest, nsrc, sizeof ndest);
	for (int i = 0; i < sizeof ndest; i++)
		printf("%d ", ndest[i]);
	printf("\n%s\n", ndest);
}
