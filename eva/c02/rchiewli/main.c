#include <unistd.h>
#include <stdio.h>
#include "ex01/ft_strncpy.c"

int	main(void)
{
	char s[20] = "ABCDEFGHIJKLMN";
	char d[20];
	ft_strncpy(d, s, 5);
	printf("[%s] \n", d);
}
