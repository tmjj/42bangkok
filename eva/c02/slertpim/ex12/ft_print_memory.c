/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: slertpim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 13:16:09 by slertpim          #+#    #+#             */
/*   Updated: 2021/10/24 13:34:25 by slertpim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

void	putchar(char c)
{
	write(1, &c, 1)
}

void	first_col()
{
}

void	second_col()
{
}

void	third_col()
{
	int	i;

	i = 0;
	while (addr[i])
	{
		if (addr[i] >= ' ' && addr <= 127)
			putchar(&addr[i]);
		else
			putchar('.');
		i++
	}
}

void	*ft_print_memory(void *addr, unsigned int size)
{
	if (size == 0)
		return (addr);
	int	line;
	int	i;

	line = 0;
	i = 0;
	while (addr[i])
	       	i++;
	if ((i + 1) % 16 == 0)
		line = (i + 1) / 16;
	else
		line = ((i + 1) / 16) + 1;
	i = 0;
	while (i < line)
	first_col();
	putchar(' ');
	second_col();
	putchar(' ');
	third_col();
	return (addr);
}

