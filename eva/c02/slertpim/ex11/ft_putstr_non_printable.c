/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: slertpim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 02:10:05 by slertpim          #+#    #+#             */
/*   Updated: 2021/10/24 15:51:51 by slertpim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr_non_printable(char *str)
{
	int		i;
	char	*hex;
	char	first;
	char	second;

	i = 0;
	hex = "0123456789abcdef";
	while (str[i])
	{
		if (str[i] >= ' ' && str[i] <= 127)
			ft_putchar(&str[i]);
		else
		{
			first = hex[str[i] / 16];
			second = hex[str[i] % 16];
			ft_putchar("\\");
			ft_putchar(first);
			ft_putchar(second);
		}	
		i++;
	}
}
