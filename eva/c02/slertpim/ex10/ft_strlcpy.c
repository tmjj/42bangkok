/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: slertpim <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 01:52:45 by slertpim          #+#    #+#             */
/*   Updated: 2021/10/24 15:49:43 by slertpim         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

unsigned int	ft_strlcpy(char *dest, char*src, unsigned int size)
{
	unsigned int	i;
	unsigned int	value;

	i = 0;
	value = 0;
	while (src[value])
	{
		value++;
	}
	while (src[i] && i < size - 1)
	{
		dest[i] = src[i];
		i++;
	}
	return (value);
}
