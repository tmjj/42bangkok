/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pleelerd <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/01 23:43:21 by pleelerd          #+#    #+#             */
/*   Updated: 2021/11/01 23:48:13 by pleelerd         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	main(int argc, char *argv[])
{
	int	k;
	int	arr_len;

	k = 0;
	argc = 0;
	arr_len = 0;
	while (argv[arr_len])
		arr_len++;
	arr_len--;
	while (argv[arr_len] && arr_len > 0)
	{
		while (argv[arr_len][k])
		{
			ft_putchar(argv[arr_len][k]);
			k++;
		}
		ft_putchar('\n');
		k = 0;
		arr_len--;
	}
}
