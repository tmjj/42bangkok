/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pleelerd <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/01 23:43:28 by pleelerd          #+#    #+#             */
/*   Updated: 2021/11/01 23:43:30 by pleelerd         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

// puts with endline appeneded
static void	ft_putendl(char *str)
{
	int		i;

	i = 0;
	while (str[i])
		ft_putchar(str[i++]);
	ft_putchar('\n');
}

// used unsigned char here probably to avoid neg/ overflow the libc also
//uses unsign char
// type casting
static int	ft_strcmp(char *s1, char *s2)
{
	while (*s1 && (*s1 == *s2))
	{
		s1 += 1;
		s2 += 1;
	}
	return (*(unsigned char *)s1 - *(unsigned char *)s2);
}
// unsigne char allows up to 255
// *(*nums)	nums[0][0]	16
// *(*nums + 1)	nums[0][1]	18
// *(*nums + 2)	nums[0][2]	20
// *(*(nums + 1))	nums[1][0]	25
// *(*(nums + 1) + 1)	nums[1][1]	26
// *(*(nums + 1) + 2)	nums[1][2]	27
// https://www.opensource.apple.com/source/Libc/Libc-262/ppc/gen/strcmp.c
//(unsigned char *)s1 typecasts s1 from a const char *s1 to a
//(unsigned char *)s1 and *(unsigned char *)s1 dereferences it to get the value.
//2nd * belongs to the type and it is the pointer qualifier.
//stackoverflow.com/questions/20106732/
//c-difference-between-unsigned-char-s1-and-unsigned-chars1
//https://ecomputernotes.com/what-is-c/function-a-pointer/
//type-casting-of-pointers

int	main(int argc, char *argv[])
{
	int		find;
	int		i;
	char	*tmp;

	find = 1;
	while (find)
	{
		find = 0;
		i = 0;
		while (++i < argc - 1)
		{
			if (ft_strcmp(argv[i], argv[i + 1]) > 0)
			{
				tmp = argv[i];
				argv[i] = argv[i + 1];
				argv[i + 1] = tmp;
				find = 1;
			}
		}
	}
	i = 0;
	while (++i < argc)
		ft_putendl(argv[i]);
	return (0);
}
// probably dont need static
