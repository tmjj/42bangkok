/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cchetana <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/02 10:10:32 by cchetana          #+#    #+#             */
/*   Updated: 2021/11/02 11:21:47 by cchetana         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_sqrt(int nb)
{
	int	sqrt;

	if (nb < 0)
		return (0);
	sqrt = 1;
	while ((unsigned int)(sqrt * sqrt) < (unsigned int)nb)
		++sqrt;
	if ((unsigned int)(sqrt * sqrt) == (unsigned int)nb)
		return (sqrt);
	return (0);
}
