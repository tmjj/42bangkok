/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cchetana <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/02 10:25:48 by cchetana          #+#    #+#             */
/*   Updated: 2021/11/02 10:29:44 by cchetana         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_prime(int nb)
{
	int	divider;

	if (nb <= 1)
		return (0);
	divider = 2;
	while (nb % divider != 0)
		++divider;
	if (nb == divider)
		return (1);
	return (0);
}
