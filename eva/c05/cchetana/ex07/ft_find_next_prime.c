/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cchetana <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/02 10:30:09 by cchetana          #+#    #+#             */
/*   Updated: 2021/11/02 10:32:35 by cchetana         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_find_next_prime(int nb)
{
	int	divider;

	divider = 2;
	while (nb % divider != 0 && nb > 1)
		++divider;
	if (divider == nb && divider != 1)
		return (nb);
	else
		return (ft_find_next_prime(nb + 1));
	return (nb);
}
