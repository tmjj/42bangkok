/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oatlasov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/01 17:01:32 by oatlasov          #+#    #+#             */
/*   Updated: 2021/11/02 12:49:11 by oatlasov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_is_prime(int nb)
{
	int	i;

	i = 2;
	if (nb == 0 || nb == 1)
		return (0);
	while (i <= (nb / i))
	{
		if (nb % i == 0)
		{
			return (0);
			break ;
		}
		i++;
	}
	return (1);
}

/*int main ()
{
	int n = 0;
	int x = ft_is_prime(n);
	printf("%d\n", x);
}*/
