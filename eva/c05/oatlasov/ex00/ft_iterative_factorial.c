/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oatlasov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 13:05:55 by oatlasov          #+#    #+#             */
/*   Updated: 2021/10/29 15:27:02 by oatlasov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_iterative_factorial(int nb)
{
	int	res;

	res = 1;
	if (nb == 0)
		return (1);
	if (nb < 0 )
		return (0);
	while (nb > 0)
	{
		res = res * nb;
		nb --;
	}
	return (res);
}

/*int	main()
{
	int x = ft_iterative_factorial(0);
	printf("%d\n", x);
}*/
