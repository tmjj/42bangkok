/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oatlasov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 14:26:02 by oatlasov          #+#    #+#             */
/*   Updated: 2021/10/29 14:58:37 by oatlasov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_iterative_power(int nb, int power)
{
	int	res;

	res = 1;
	if (power < 0)
		return (0);
	if (power == 0)
		return (1);
	while (power != 0)
	{
		res = res * nb;
		power--;
	}
	return (res);
}

/*int main()
{
	int a = 0;
	int b = 3;
	int x = ft_iterative_power(a,b);
	printf("%d\n", x);
}*/
