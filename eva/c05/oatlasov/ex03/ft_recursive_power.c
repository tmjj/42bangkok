/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oatlasov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 14:59:41 by oatlasov          #+#    #+#             */
/*   Updated: 2021/10/29 15:19:56 by oatlasov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_recursive_power(int nb, int power)
{
	if (power < 0)
		return (0);
	if (power == 0)
		return (1);
	if (power == 1)
		return (nb);
	while (power != 0)
	{
		return (nb * ft_recursive_power(nb, power - 1));
	}
	return (nb);
}

/*int main()
{
	int a = 4;
	int b = 5;
	int x = ft_recursive_power(a,b);
	printf("%d\n", x);
}*/
