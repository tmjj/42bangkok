/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_pointer.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cananwat <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 15:54:21 by cananwat          #+#    #+#             */
/*   Updated: 2021/10/23 18:58:32 by cananwat         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	main(void)
{
	char	db[10];
	char	*p;
	int		xxx;

	xxx = 0;
	p = &db[0];
	while (p != &db[5])
	{
		ft_putchar(*p + '0');
		p++;
	}
	return (0);
}
